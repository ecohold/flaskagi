# BotAGI
BotAGI é uma API para classificação e geração de texto usando modelos de linguagem. Esta API recebe solicitações de texto, classifica e gera respostas usando dois modelos de linguagem diferentes (BERT e GPT-2), comparando suas acurácias para determinar a melhor resposta.

- Requisitos
Python 3.6 ou superior
Flask 2.1.1
Transformers 4.39.3
Pillow 10.3.0
TinyDB 4.5.1

- Estrutura do Projeto

        FlaskAGI/
        │
        ├── app.py
        ├── acuracias.json
        └── requirements.txt

app.py: O arquivo principal que contém a implementação da API.
acuracias.json: Banco de dados JSON para armazenar as acurácias das respostas.
requirements.txt: Arquivo de requisitos do Python para instalar as dependências.

- API Documentation
A documentação da API segue o padrão OpenAPI (Swagger) e pode ser encontrada no arquivo openapi.yaml. Aqui está um resumo das principais rotas:

POST /text: Rota para enviar texto para classificação e geração.
text: O texto a ser enviado na solicitação.
best_response: A melhor resposta classificada ou gerada.
bert_accuracy: A acurácia da resposta classificada pelo modelo BERT.
gpt2_accuracy: A acurácia da resposta gerada pelo modelo GPT-2.

- Uso
Clone o repositório:

git clone https://gitlab.com/ecohold/flaskagi
Instale as dependências:

pip install -r requirements.txt
Execute a aplicação:

- Contribuição
Contribuições são bem-vindas! Sinta-se à vontade para abrir um problema ou enviar um pull request.

- Licença
Pede licença pro santo e já era.

